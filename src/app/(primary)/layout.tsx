import Header from "@/components/Header";
import Footer from "@/components/Footer";
import ActionToolbar from "@/components/ActionToolbar";

export default function MainLayout({ children }: { children: React.ReactNode }) {
  return (
    <div>
      <Header />
      <main>{children}</main>
      <Footer />
      <ActionToolbar />
    </div>
  );
}
