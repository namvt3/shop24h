import Home from "@/components/Home";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import "./style.scss";

export default function HomePage() {
  return <Home />;
}
