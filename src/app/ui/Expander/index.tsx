"use client";

import { useState } from "react";
import "./style.scss";

export default function Expander({
  className = "",
  label,
  children,
}: {
  className?: string;
  label: string | JSX.Element;
  children: JSX.Element;
}) {
  const [expanded, setExpanded] = useState(false);
  const [rendered, setRendered] = useState(false);

  const handleClick = () => {
    if (expanded) {
      setTimeout(() => setRendered(false), 500);
      setExpanded(false);
    } else {
      setRendered(true);
      setExpanded(true);
    }
  };

  return (
    <div className={`expander ${expanded ? "expanded" : ""} ${className}`}>
      <div className="expander__label" onClick={handleClick}>
        {label}
      </div>
      <div className='expander__content'>{rendered && children}</div>
    </div>
  );
}
