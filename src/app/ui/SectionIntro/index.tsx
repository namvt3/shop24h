import './style.scss';

export default function SectionInto({ intro }: { intro: string }) {
  return <p className="section-intro">{intro}</p>;
}
