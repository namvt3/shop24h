export const formatMoney = (number: number): string => {
    return `${number.toLocaleString()}₫`
}