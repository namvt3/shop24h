export const items = [
  {
    image: "https://file.hstatic.net/1000324420/article/blog-img-2_large.jpg",
    href: "/blogs/tu-che-mon-thach-sua-chua-thanh-long-lung-linh-sac-mau",
    name: "Tự chế món thạch sữa chua thanh long lung linh sắc màu",
    summary:
      "Thạch sữa chua thanh long là món ăn tráng miệng tuyệt vời cho các mẹ. Đặc biệt là các bạn trẻ. Bởi vì món ăn này rất thanh mát, dễ ăn, đẹp da và trông rất màu sắc...",
  },
  {
    image: "https://file.hstatic.net/1000324420/article/blog-img-5_large.jpg",
    href: "/blogs/ky-thuat-trong-rau-sach-trong-chau-xop-tai-nha-don-gian",
    name: "Kỹ thuật trồng rau sạch trong chậu xốp tại nhà đơn giản",
    summary:
      "Tự trồng rau trong thùng xốp tại nhà là sự lựa chọn của rất nhiều gia đình trong thành phố bởi phương pháp trồng rau đơn giản, dễ trồng, dễ quản lý, an toàn và tiện...",
  },
  {
    image: "https://file.hstatic.net/1000324420/article/blog-img-3_large.jpg",
    href: "/blogs/vi-sao-hoa-qua-viet-that-the-truoc-con-loc-hang-nhap-ngoai",
    name: "Vì sao hoa quả Việt thất thế trước “cơn lốc” hàng nhập ngoại?",
    summary:
      "Rau củ quả Việt Nam đang dần khẳng định tên tuổi của mình trên thị trường quốc tế khi nhiều mặt hàng đã có “visa” vào các thị trường khó tính như Mỹ, Nhật Bản, Australia…...",
  },
];
