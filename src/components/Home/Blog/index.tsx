"use client";

import SectionTitle from "@/app/ui/SectionTitle";
import "./style.scss";
import Slider from "react-slick";
import Image from "next/image";
import { items } from "./fixedData";
import Link from "next/link";
import { MouseEvent, useState } from "react";
import SectionInto from "@/app/ui/SectionIntro";

export default function Blog() {
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 542,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  const [dragging, setDragging] = useState(false);

  const handleBeforeChange = () => {
    setDragging(true);
  };

  const handleAfterChange = () => {
    setDragging(false);
  };

  const handleOnItemClick = (e: MouseEvent<HTMLElement>) => {
    if (dragging) e.preventDefault(); // Prevent href tag to open when you dragging
  };

  return (
    <div className="blog">
      <SectionTitle title="Tin cập nhật" />
      <SectionInto intro="Tin tức vệ sinh an toàn thực phẩm cập nhật mới nhất mỗi ngày cho bạn" />

      <Slider
        {...settings}
        className="blog__slider gray-arrows"
        beforeChange={handleBeforeChange}
        afterChange={handleAfterChange}
      >
        {items.map((item, index) => (
          <div key={index} className="blog__slider__item">
            <Link href={item.href}>
              <Image
                unoptimized
                priority
                width={480}
                height={274}
                src={item.image}
                alt={item.name}
                onClick={handleOnItemClick}
              />
            </Link>
            <div className="blog-item-info">
              <h3 className="blog-item-name">
                <Link href={item.href}>{item.name}</Link>
              </h3>
              <p className="blog-item-summary">{item.summary}</p>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
}
