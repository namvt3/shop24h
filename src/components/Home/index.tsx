import BackToTop from "@/app/ui/BackToTop";
import Banners from "./Banners";
import Blog from "./Blog";
import Brands from "./Brands";
import Carousel from "./Carousel";
import Collection from "./Collection";
import Policy from "./Policy";
import Testimonial from "./Testimonial";
import { items } from "./fixedData";
import "./style.scss";
import SidebarCategory from "./SidebarCategory";

export default function Home() {
  return (
    <div className="home-page">
      <section className="home-page__carousel-row">
        <SidebarCategory />
        <Carousel />
      </section>
      <Banners />
      <Collection title="Trái cây mỗi ngày" items={items} />
      <Collection title="Rau tươi mới" items={items} />
      <Collection title="Thực phẩm khô" items={items} />
      <Policy />
      <Blog />
      <Testimonial />
      <Brands />
      <BackToTop />
    </div>
  );
}
