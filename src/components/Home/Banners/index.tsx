"use client";

import Image from "next/image";
import Slider from "react-slick";
import "./style.scss";

export default function Banners() {
  const imgUrls = [
    "https://theme.hstatic.net/1000324420/1000664192/14/banner1.jpg?v=52",
    "https://theme.hstatic.net/1000324420/1000664192/14/banner2.jpg?v=52",
    "https://theme.hstatic.net/1000324420/1000664192/14/banner3.jpg?v=52",
  ];

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          dots: true,
        },
      },
      {
        breakpoint: 542,
        settings: {
          slidesToShow: 1,
          dots: true,
        },
      },
    ],
  };

  return (
    <div className="banners">
      <Slider {...settings} className="banners__slider">
        {imgUrls.map((item, index) => (
          <Image priority unoptimized width={360} height={156} key={index} src={item} alt="" />
        ))}
      </Slider>
    </div>
  );
}
