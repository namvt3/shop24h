const categories = [
  {
    name: "Rau tươi sạch",
    href: "/collections/rau-tuoi-sach",
  },
  {
    name: "Thịt tươi sạch",
    href: "/collections/thit-tuoi-sach",
  },
  {
    name: "Hải sản tươi sống",
    href: "/collections/hai-san-tuoi-song",
  },
  {
    name: "Hải sản nhập khẩu",
    href: "/collections/hai-san-nhap-khau",
  },
  {
    name: "Trái cây miền nam",
    href: "/collections/trai-cay-mien-nam",
  },
  {
    name: "Hoa quả sạch",
    href: "/collections/hoa-qua-sach",
  },
  {
    name: "Hàng nhập khẩu",
    href: "/collections/hang-nhap-khau",
  },
  {
    name: "Rau quả Đà Lạt",
    href: "/collections/rau-qua-da-lat",
  },
  {
    name: "Sản phẩm nổi bật",
    href: "/collections/san-pham-noi-bat",
  },
  {
    name: "Sản phẩm khuyến mãi",
    href: "/collections/san-pham-khuyen-mai",
  },
  {
    name: "Tất cả sản phẩm",
    href: "/collections/all",
  },
];

export default categories;
