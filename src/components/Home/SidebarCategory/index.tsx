import Link from "next/link";
import "./style.scss";
import ArrowRightSVG from "@/assets/ArrowRight";
import categories from "./categories";

export default function SidebarCategory() {
  return (
    <aside className="sidebar-category">
      <div className="sidebar-category__title">Danh mục</div>
      <ul className="sidebar-category__content">
        {categories.map((item) => (
          <li key={item.name}>
            <Link href={item.href}>
              <ArrowRightSVG />
              {item.name}
            </Link>
          </li>
        ))}
      </ul>
    </aside>
  );
}
