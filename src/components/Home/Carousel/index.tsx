"use client";

import Image from "next/image";
import Slider from "react-slick";
import "./style.scss";

export default function Carousel() {
  const imgUrls = [
    "https://theme.hstatic.net/1000324420/1000664192/14/slider_1.jpg?v=52",
    "https://theme.hstatic.net/1000324420/1000664192/14/slider_2.jpg?v=52",
  ];

  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true
  };

  return (
    <div className="carousel">
      <Slider {...settings} className="carousel__slider">
        {imgUrls.map((item, index) => (
          <Image
            unoptimized
            width={847}
            height={412}
            key={index}
            src={item}
            alt=""
          />
        ))}
      </Slider>
    </div>
  );
}
