"use client";

import Image from "next/image";
import Slider from "react-slick";
import "./style.scss";
import { formatMoney } from "@/app/utilities/_functions";
import Link from "next/link";
import { MouseEvent, useState } from "react";
import SectionTitle from "@/app/ui/SectionTitle";

interface Item {
  id: string;
  slug?: string;
  img: string;
  name: string;
  price: number;
  discount?: number;
  newPrice?: number;
}

export default function Collection({ title, items }: { title: string; items: Item[] }) {
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
    pauseOnDragging: true,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const [dragging, setDragging] = useState(false);

  const handleBeforeChange = () => {
    setDragging(true);
  };

  const handleAfterChange = () => {
    setDragging(false);
  };

  const handleOnItemClick = (e: MouseEvent<HTMLElement>) => {
    if (dragging) e.preventDefault(); // Prevent href tag to open when you dragging
  };

  return (
    <div className="collection">
      <SectionTitle title={title} />

      <Slider
        {...settings}
        className="collection__slider gray-arrows"
        beforeChange={handleBeforeChange}
        afterChange={handleAfterChange}
      >
        {items.map((item: Item, index: number) => (
          <div key={index} className="product-box">
            <Link
              href={`/product/${item.slug ? item.slug + "/" : ""}${item.id}`}
              onClick={handleOnItemClick}
            >
              <Image unoptimized width={240} height={240} src={item.img} alt={item.name} />
            </Link>

            {item.discount && <div className="sale-flash">-{item.discount}%</div>}

            <div className="product-info">
              <Link
                href={`/product/${item.slug ? item.slug + "/" : ""}${item.id}`}
                className="product-info__name"
              >
                {item.name}
              </Link>
              <div className="product-info__price">
                <div className="new-price">{formatMoney(item.newPrice || item.price)}</div>
                {item.newPrice && <div className="old-price">{formatMoney(item.price)}</div>}
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
}
