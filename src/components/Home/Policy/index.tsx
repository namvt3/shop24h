"use client";

import Image from "next/image";
import './style.scss'

export default function Policy() {
  return (
    <div className="policy">
      <Image
        src="https://theme.hstatic.net/1000324420/1000664192/14/banner_coltab3_1.png?v=52"
        alt=""
        width={555}
        height={160}
        unoptimized
      />
      <Image
        src="https://theme.hstatic.net/1000324420/1000664192/14/banner_coltab3_2.png?v=52"
        alt=""
        width={555}
        height={160}
        unoptimized
      />
    </div>
  );
}
