"use client";

import SectionInto from "@/app/ui/SectionIntro";
import SectionTitle from "@/app/ui/SectionTitle";
import Slider from "react-slick";
import "./style.scss";
import Image from "next/image";

export default function Testimonial() {
  const fixedData = [
    {
      image: "https://theme.hstatic.net/1000324420/1000664192/14/testimonial_1.jpg?v=52",
      name: "Người mẫu - Ngọc Trinh",
      designation:
        "Là một người khá kỹ tính, tôi luôn luôn lựa chọn những thực phẩm sạch nhất. Và đây là nơi tôi đặt trọn niềm tin.",
    },
    {
      image: "https://theme.hstatic.net/1000324420/1000664192/14/testimonial_2.jpg?v=52",
      name: "Diễn viên - Phương Trinh",
      designation:
        "Là một người khá kỹ tính, tôi luôn luôn lựa chọn những thực phẩm sạch nhất. Và đây là nơi tôi đặt trọn niềm tin.",
    },
    {
      image: "https://theme.hstatic.net/1000324420/1000664192/14/testimonial_3.jpg?v=52",
      name: "Ca sĩ - Hoàng Yến",
      designation:
        "Là một người khá kỹ tính, tôi luôn luôn lựa chọn những thực phẩm sạch nhất. Và đây là nơi tôi đặt trọn niềm tin.",
    },
  ];

  const settings = {
    infinite: false,
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 542,
        settings: {
          slidesToShow: 1
        }
      },
    ],
  };

  return (
    <div className="testimonial">
      <SectionTitle title="phản hồi của khách" />
      <SectionInto intro="Phản hồi của những khách hàng đã và đang sử dụng sản phẩm trong suốt những năm qua" />

      <Slider {...settings} className="gray-arrows">
        {fixedData.map((item, index) => (
          <div key={index} className="testimonal-item">
            <Image
              src={item.image}
              alt={item.name}
              width={80}
              height={80}
              onLoad={() => item.image}
              unoptimized
            />
            <h4>{item.name}</h4>
            <p>{item.designation}</p>
          </div>
        ))}
      </Slider>
    </div>
  );
}
