"use client";

import Image from "next/image";
import Slider from "react-slick";

export default function Brands() {
  const fixedData = [
    "https://theme.hstatic.net/1000324420/1000664192/14/brand1.png?v=52",
    "https://theme.hstatic.net/1000324420/1000664192/14/brand2.png?v=52",
    "https://theme.hstatic.net/1000324420/1000664192/14/brand1.png?v=52",
    "https://theme.hstatic.net/1000324420/1000664192/14/brand2.png?v=52",
  ];
  const settings = {
    variableWidth: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    speed: 2500,
    autoplaySpeed: 2500,
    cssEase: "linear",
    pauseOnHover: false,
  };

  return (
    <div className="brands">
      <Slider {...settings}>
        {fixedData.map((item) => (
          <Image key={item} src={item} alt="brand-logo" width={165} height={80} unoptimized />
        ))}
      </Slider>
    </div>
  );
}
