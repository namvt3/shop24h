import Expander from "@/app/ui/Expander/";
import "./style.scss";
import PhoneIcon from "@/assets/PhoneIcon";
import MailIcon from "@/assets/Envelope";
import LocationIcon from "@/assets/Location";

export default function Footer() {
  return (
    <div className="footer">
      <div className="footer__content">
        <Expander label={<h3>LIÊN HỆ</h3>} className="footer--contact">
          <div>
            Chúng tôi chuyên cung cấp các sản phẩm thực phẩm sạch an toàn cho sức khỏe con người
            <ul>
              <li>
                <LocationIcon fill="#fe9705" />
                268 Cầu Giấy, Quận Cầu Giấy, Hà Nội, Vietnam
              </li>
              <li>
                <PhoneIcon fill="#fe9705" />
                0912117494 Thứ 2 - Chủ nhật: 9:00 - 18:00
              </li>
              <li>
                <MailIcon fill="#fe9705" />
                dualeotheme@gmail.com
              </li>
            </ul>
          </div>
        </Expander>
        <Expander label={<h3>DANH MỤC</h3>}>
          <div>
          </div>
        </Expander>
        <Expander label={<h3>HỖ TRỢ KHÁCH HÀNG</h3>}>
          <div></div>
        </Expander>
        <Expander label={<h3>KẾT NỐI VỚI DUALEO</h3>}>
          <div></div>
        </Expander>
      </div>
    </div>
  );
}
