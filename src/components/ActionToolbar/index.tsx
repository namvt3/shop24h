import "./style.scss";
import PhoneIcon from "@/assets/PhoneIcon";
import MailIcon from "@/assets/Envelope";
import LocationIcon from "@/assets/Location";
import ZaloIcon from "@/assets/Zalo";
import MessengerIcon from "@/assets/Messenger";
import Image from "next/image";
import PhoneCircle from "@/assets/PhoneCircle.svg";
import ZaloCircle from "@/assets/ZaloCircle.svg";
import MessengerCircle from "@/assets/MessengerCircle.svg";
import MailCircle from "@/assets/MailCircle.svg";
import LocationCircle from "@/assets/LocationCircle.svg"

export default function ActionToolbar() {
  const fillColor = "#696969";

  return (
    <div className="action-toolbar">
      <div className="action-toolbar--desktop">
        <Image src={PhoneCircle} width={44} height={44} alt="phone"/>
        <Image src={ZaloCircle} width={44} height={44} alt="zalo"/>
        <Image src={MessengerCircle} width={44} height={44} alt="messenger"/>
        <Image src={MailCircle} width={44} height={44} alt="mail"/>
        <Image src={LocationCircle} width={44} height={44} alt="contact"/>
      </div>
      <div className="action-toolbar--mobile">
        <PhoneIcon width={24} height={24} fill={fillColor} />
        <ZaloIcon width={24} height={24} fill={fillColor} />
        <MessengerIcon width={48} height={48} className="messenger-icon" />
        <MailIcon width={24} height={24} fill={fillColor} />
        <LocationIcon width={24} height={24} fill={fillColor} />
      </div>
    </div>
  );
}
