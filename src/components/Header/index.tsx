"use client";

import "./style.scss";
import Image from "next/image";
import MobilePhone from "@/assets/MobilePhone.svg";
import LocationIcon from "@/assets/Location";
import Link from "next/link";
import UserIcon from "@/assets/User";
import ShoppingBagIcon from "@/assets/ShoppingBag";
import { usePathname } from "next/navigation";

export default function Header() {
  const pathname = usePathname();
  const navLeft = [
    {
      href: "/",
      name: "Trang chủ",
    },
    {
      href: "/collections/all",
      name: "Sản phẩm",
    },
    {
      href: "/blogs/news",
      name: "Tin tức",
    },
    {
      href: "/about-us",
      name: "Giới thiệu",
    },
    {
      href: "/contact",
      name: "Liên hệ",
    },
  ];

  return (
    <header className="header">
      <div className="header__topbar">
        <ul className="header__topbar__phone-and-address">
          <li>
            <Image src={MobilePhone} width={20} height={20} alt="phone" />
            Hotline:&nbsp;<b>1900 6750</b>
          </li>
          <li className="header-address">
            <LocationIcon fill="white" />
            &nbsp;Địa chỉ: Ladeco Building, 266 Doi Can Street, Hà Nội
          </li>
        </ul>
        <div className="header-login">
          <UserIcon fill="white" />
          &nbsp;
          <Link href={""}>Đăng nhập</Link>&nbsp;hoặc&nbsp;<Link href={""}>đăng ký</Link>
        </div>
      </div>

      <div className="header__content">
        <button className="header-hamburger" type="button">
          <Image src="/burger-menu.svg" alt="menu" width={24} height={24} priority />
        </button>

        <Image src="/logo.png" alt="Shop24h Logo" width={213} height={53} priority />

        <div className="header-item-policy">
          <Image
            src="https://theme.hstatic.net/1000324420/1000664192/14/policy1.png?v=52"
            onLoad={() => "https://theme.hstatic.net/1000324420/1000664192/14/policy1.png?v=52"}
            width={43}
            height={30}
            alt=""
            unoptimized
          />
          <b>Miễn phí vận chuyển</b>
          <span>Bán kính 100km</span>
        </div>

        <div className="header-item-policy">
          <Image
            src="https://theme.hstatic.net/1000324420/1000664192/14/policy2.png?v=52"
            onLoad={() => "https://theme.hstatic.net/1000324420/1000664192/14/policy2.png?v=52"}
            width={34}
            height={34}
            alt=""
            unoptimized
          />
          <b>Hỗ trợ 24/7</b>
          <span>
            Hotline: <b>1900 6750</b>
          </span>
        </div>

        <div className="header-item-policy">
          <Image
            src="	https://theme.hstatic.net/1000324420/1000664192/14/policy3.png?v=52"
            onLoad={() => "	https://theme.hstatic.net/1000324420/1000664192/14/policy3.png?v=52"}
            width={35}
            height={35}
            alt=""
            unoptimized
          />
          <b>Giờ làm việc</b>
          <span>T2 - T7 Giờ hành chính</span>
        </div>

        <div className="header-cart--desktop">
          <ShoppingBagIcon width={24} height={24} fill="white" />
          Giỏ hàng (0)
        </div>

        <div className="header-cart--mobile">
          <button>
            <ShoppingBagIcon width={24} height={24} />
          </button>
          <div className="cart-badge center">0</div>
        </div>
      </div>

      <div className="header__navbar">
        <ul className="nav-left">
          {navLeft.map((item) => (
            <li key={item.href}>
              <Link href={item.href} className={`${pathname === item.href && "active"}`}>
                {item.name}
              </Link>
            </li>
          ))}
        </ul>
        <div className="menu-search"></div>
      </div>
    </header>
  );
}
